package com.natcha.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clickedHelloButton()
    }
    private fun clickedHelloButton() {
        val helloButton: Button = findViewById(R.id.buttonHello)
        helloButton.setOnClickListener {
            logging()
            sendText()
        }
    }
    private fun sendText() {
        val nameTextView: TextView = findViewById(R.id.profileName)
        val name: String = nameTextView.text.toString()
        val intent = Intent(this,HelloActivity::class.java)
        intent.putExtra("ProfileName", name)
        startActivity(intent)
    }
    private fun logging() {
        Log.d(TAG, getString(R.string.name))
        Log.d(TAG, getString(R.string.stuID))
    }
    companion object {
        private const val TAG = "MainActivity"
    }
}