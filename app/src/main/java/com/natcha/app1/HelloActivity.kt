package com.natcha.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val profileName=intent.getStringExtra("ProfileName")
        val name: TextView = findViewById<TextView?>(R.id.textView2)
        name.text = profileName
    }
}